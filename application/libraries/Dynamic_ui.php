<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// ==================================================================
//
// Copyright (C) 2016 Carson Chew <chew_0212@hotmail.com>
//
// Everyone is permitted to copy and distribute verbatim or modified
// copies of this license document, and changing it is allowed as long
// as the name is changed.
//
// ------------------------------------------------------------------



class Dynamic_ui {

	protected $ci;

	public function __construct(){
        $this->ci =& get_instance();
        $this->ci->load->database();
	}



	// ==================================================================
	//
	// Select list
	//
	// ------------------------------------------------------------------

	/**
	 * Implement edit select List
	 */
	public function edit_select_list($base_db_name, $column_one, $where_id, $output_name, $vocabulary_category = "", $title = null, $extra_value=null){

		$db_name = $this->ci->db->dbprefix($base_db_name);

		//For display the selected value
		$sql_selected = $this->ci->db->query("SELECT * FROM $db_name WHERE $column_one = '$where_id'  $vocabulary_category");
		$exits_row = $sql_selected->row_array();

		if (!empty($extra_value)) {
			$extra_valx = (!empty($exits_row["$extra_value"])) ? " - ".$exits_row["$extra_value"] : null ;
		}else{
			$extra_valx = null;
		}

		echo "<option value='" .$exits_row["$column_one"]. "' selected>" .$exits_row["$output_name"].$extra_valx."</option>".PHP_EOL;

		$sql = $this->ci->db->query("SELECT * FROM $db_name WHERE $column_one <> '$where_id'  $vocabulary_category ORDER BY $output_name");

		foreach ($sql->result_array() as $row){

			if (!empty($extra_value) && $extra_value != null) {
				$extra_valuex = (!empty($row["$extra_value"])) ? " - ".$row["$extra_value"] : null ;
			}else{
				$extra_valuex = null;
			}

			$title = ($title == null)? "": $row["$title"];

			echo "<option value='" .$row["$column_one"]. "' title='".$title."'>" .$row["$output_name"].$extra_valuex."</option>".PHP_EOL;
		}
	}

	/**
	 * Implement edit select List, but show selected data only, accept $where_id parameter as array only
	 */
	public function edit_select_list_array($base_db_name, $column_one, $where_id, $output_name, $vocabulary_category = "", $title = null, $extra_value=null){

		$db_name = $this->ci->db->dbprefix($base_db_name);

		$arr_where_id = explode(',', $where_id);

		foreach ($arr_where_id as $key => $row) {
			//For display the selected value
			$sql_selected = $this->ci->db->query("SELECT * FROM $db_name WHERE $column_one = '$row'  $vocabulary_category");
			$exits_row = $sql_selected->row_array();

			if (!empty($extra_value)) {
				$extra_valx = (!empty($exits_row["$extra_value"])) ? " - ".$exits_row["$extra_value"] : null ;
			}else{
				$extra_valx = null;
			}
	
			echo "<option value='" .$exits_row["$column_one"]. "' selected>" .$exits_row["$output_name"].$extra_valx."</option>".PHP_EOL;		

			if ($key == 0) {
				$where_query = "WHERE $column_one <> '$row' ";
			} else {
				$where_query .= "AND $column_one <> '$row'";
			}
		}

		$sql = $this->ci->db->query("SELECT * FROM $db_name $where_query $vocabulary_category ORDER BY $output_name");

		foreach ($sql->result_array() as $row){

			if (!empty($extra_value) && $extra_value != null) {
				$extra_valuex = (!empty($row["$extra_value"])) ? " - ".$row["$extra_value"] : null ;
			}else{
				$extra_valuex = null;
			}

			$title = ($title == null)? "": $row["$title"];

			echo "<option value='" .$row["$column_one"]. "' title='".$title."'>" .$row["$output_name"].$extra_valuex."</option>".PHP_EOL;
		}
	
	}


	/**
	 * Check the multiple table
	 */
	public function detect_multiple_table($table_name){

		$combine = array();

		if(strpos($table_name, ',') !== false){

			$exp = explode(',', $table_name);

			foreach($exp as $raw_name){
				$full_db_name = $this->ci->db->dbprefix($raw_name);

				$combine[] = $full_db_name;
			}

			return implode(",", $combine);

		} else {
			return $this->ci->db->dbprefix($table_name);;
		}
	}


	/**
	 * Implement load all data - select List
	 */
	public function load_all_select_list($base_db_name, $output_name, $where_id, $where="", $title=null, $extra_value=null, $order_by = null, $extra_text = null, $spliter = " - "){

		$db_name = $this->detect_multiple_table($base_db_name);

		if ($order_by == null) {
			$tax= $this->ci->db->query("SELECT * FROM $db_name $where ORDER BY $output_name");
		} else {
			$tax= $this->ci->db->query("SELECT * FROM $db_name $where ORDER BY $order_by");
		}

		if ($tax->num_rows() == 0) {
			return false;
		} else {

			foreach ($tax->result() as $result){

				if (!empty($extra_value) && $extra_value != null) {
					$extra_valuex = (!empty($result->$extra_value)) ? $extra_text.$result->$extra_value : null ;
				}else{
					$extra_valuex = null;
				}

				$id = $result->$where_id;
				$title = ($title != null)? $result->$title: "";


				if (!empty($result->$output_name) && $result->$output_name != null) {
					$spliter_out = ($extra_valuex != null) ? $spliter : null ;

					$output = $result->$output_name.$spliter_out;
				}else{
					$output = null;
				}

				echo"<option value='".$id."' title='".$title."'>".$output.$extra_valuex."</option>".PHP_EOL;
			}

		}
	}



	/**
	 * Implement edit select List, but show selected data only
	 */
	public function edit_selected_list($base_db_name, $column_one, $where_id, $output_name, $vocabulary_category = "", $title = null, $extra_value=null){

		$db_name = $this->ci->db->dbprefix($base_db_name);

		//For display the selected value
		$sql_selected = $this->ci->db->query("SELECT * FROM $db_name WHERE $column_one = '$where_id'  $vocabulary_category");
		$exits_row = $sql_selected->row_array();

		if (!empty($extra_value)) {
			$extra_valx = (!empty($exits_row["$extra_value"])) ? " - ".$exits_row["$extra_value"] : null ;
		}else{
			$extra_valx = null;
		}

		echo "<option value='" .$exits_row["$column_one"]. "' selected>" .$exits_row["$output_name"].$extra_valx."</option>".PHP_EOL;
	}



	/**
	 * Load the data as radio button
	 */
	public function load_all_radio($base_db_name, $where, $ordering, $where_id, $output_name, $radio_name, $radio_inline = null, $default_check = null, $required = null){

		$db_name = $this->ci->db->dbprefix($base_db_name);

		$tax = $this->ci->db->query("SELECT * FROM $db_name $where ORDER BY $ordering");

		if ($tax->num_rows() == 0) {
			return false;
		} else {

			$radio_inline = ($radio_inline != null) ? "-inline" : null ;

			foreach ($tax->result() as $result){

				$id = $result->$where_id;
				$output = $result->$output_name;
				$checked = ($default_check == $id) ? "checked" : null ;
				$required = ($required != null) ? "required" : null ;

				echo '<label class="radio'.$radio_inline.'">
						<input type="radio" name="'.$radio_name.'" id="'.$radio_name.'" value="'.$id.'" '.$checked.' '.$required.'>'.$output.'
					  </label>'.PHP_EOL;
			}

		}
	}


	/**
	 * Load the data as checkbox
	 */
	public function load_all_chkbox($base_db_name, $where, $ordering, $where_id, $output_name, $chkbox_name, $chkbox_inline = null, $default_check = null, $required = null){

		$db_name = $this->ci->db->dbprefix($base_db_name);

		$tax = $this->ci->db->query("SELECT * FROM $db_name $where ORDER BY $ordering");

		if ($tax->num_rows() == 0) {
			return false;
		} else {

			$chkbox_inline = ($chkbox_inline != null) ? "-inline" : null ;

			foreach ($tax->result() as $result){

				$id = $result->$where_id;
				$output = $result->$output_name;
				$checked = ($default_check == $id) ? "checked" : null ;
				$required = ($required != null) ? "required" : null ;

				echo '<label class="checkbox'.$chkbox_inline.'">
						<input type="checkbox" name="'.$chkbox_name.'[]" id="'.$chkbox_name.'" value="'.$id.'" '.$checked.' '.$required.'>'.$output.'
					  </label>'.PHP_EOL;
			}

		}
	}


	/**
	 * Edit check box checkbox
	 */
	public function edit_chkbox($base_db_name, $where, $ordering, $where_id, $output_name, $chkbox_name, $chkbox_inline = null, $default_check = null, $required = null){


		$db_name = $this->ci->db->dbprefix($base_db_name);

		$chkbox_inline = ($chkbox_inline != null) ? "-inline" : null ;

		if ($default_check != null) {

			// Load selected first
			$load_selected = $this->ci->db->query("SELECT * FROM $db_name WHERE $where_id IN ($default_check) $where ORDER BY $ordering");

			if ($load_selected->num_rows() == 0) {
				return false;
			} else {

				foreach ($load_selected->result() as $selected_result){

					$selected_id = $selected_result->$where_id;
					$selected_output = $selected_result->$output_name;

					echo '<label class="checkbox'.$chkbox_inline.'">
							<input type="checkbox" name="'.$chkbox_name.'[]" id="'.$chkbox_name.'" value="'.$selected_id.'" checked>'.$selected_output.'
						  </label>'.PHP_EOL;
				}
			}

		}


		$new_default_check = ($default_check != null) ? $default_check : 0 ;

		$tax = $this->ci->db->query("SELECT * FROM $db_name WHERE $where_id NOT IN ($new_default_check) $where ORDER BY $ordering");

		if ($tax->num_rows() == 0) {
			return false;
		} else {

			foreach ($tax->result() as $result){

				$id = $result->$where_id;
				$output = $result->$output_name;
				$checked = ($default_check == $id) ? "checked" : null ;
				$required = ($required != null) ? "required" : null ;

				echo '<label class="checkbox'.$chkbox_inline.'">
						<input type="checkbox" name="'.$chkbox_name.'[]" id="'.$chkbox_name.'" value="'.$id.'" '.$checked.' '.$required.'>'.$output.'
					  </label>'.PHP_EOL;
			}

		}

	}


	/**
	 * Implement edit taxonomy with multiple value
	 */
	public function edit_multiple_tax($base_db_name, $con_field, $array_set, $field_value, $vocabulary_category = ""){

		$db_name = $this->ci->db->dbprefix($base_db_name);

		$foo = explode(",", $array_set);

		foreach($foo as $array) {

			$tax = $this->ci->db->query("SELECT * FROM $db_name WHERE $con_field IN ($array) $vocabulary_category");
			$r = $tax->row();

			echo "<option value='" .$r->$con_field. "' selected>" .$r->$field_value."</option>";
		}


		$sql = $this->ci->db->query("SELECT * FROM $db_name WHERE $con_field NOT IN ($array_set) $vocabulary_category ORDER BY $field_value");

		foreach ($sql->result() as $row) {
			echo "<option value='" .$row->$con_field. "'>" .$row->$field_value."</option>";
		}

	}



	// Own array select list
	public function load_array_tax($all_array){

		foreach($all_array as $array) {
			echo "<option value='" .$array. "'>" .$array."</option>";
		}
	}


	// Edit Own array select list
	public function edit_array_tax($all_array, $where_id){

		foreach($all_array as $array) {

			$selected = ($array == $where_id) ? "selected" : null ;

			echo "<option value='" .$array. "' ".$selected.">" .$array."</option>";
		}
	}


	// Edit Own array select list but the where id is array
	public function edit_array_tax_array_key($all_array, $where_id){

		if (!empty($where_id)) {
			$new_where_array = explode(',', $where_id);
			$clean_where_id = array_unique($new_where_array);
		}else{
			$clean_where_id = null;
		}

		foreach($all_array as $array) {

			$selected = (in_array($array, $clean_where_id)) ? "selected" : null ;

			echo "<option value='" .$array. "' ".$selected.">" .$array."</option>";
		}
	}

}

/* End of file Dynamic_ui.php */
/* Location: ./application/libraries/Dynamic_ui.php */
