<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// ==================================================================
//
// Copyright (C) 2016 Carson Chew <chew_0212@hotmail.com>
//
// Everyone is permitted to copy and distribute verbatim or modified
// copies of this license document, and changing it is allowed as long
// as the name is changed.
//
// ------------------------------------------------------------------


/**
 * Get today date with time
 */
function today_date(){

  $date = date('Y-m-d H:i:s');

  return $date;
}


/**
 * Get today date
 */
function current_date(){

  $date = date('Y-m-d');

  return $date;
}


/**
 * Trim date
 * "F d, Y h:i:s A" - March 09, 2018 01:41:57 PM
 */
function trim_date($full_date, $date_formate, $current_date_format = "Y-m-d H:i:s", $msg = null){

  if ($full_date == "0000-00-00 00:00:00") {
    $result = ($msg == null) ? "N/A" : $msg ;
    return $result;
  } else {
    $date = DateTime::createFromFormat($current_date_format, $full_date);
    $foo = $date->format($date_formate);

    return $foo;
  }
}


// convert timestamps to date
function convert_date($timestamp){
	return $todays = Date("Y-m-d", strtotime($timestamp));
}


//////////////////////////////////////////////////////////////////////
// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds' =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
// '%y Year %m Month %d Day'                               =>  1 Year 3 Month 14 Days
// '%m Month %d Day'                                       =>  3 Month 14 Day
// '%d Day %h Hours'                                       =>  14 Day 11 Hours
// '%d Day'                                                =>  14 Days
// '%h Hours %i Minute %s Seconds'                         =>  11 Hours 49 Minute 36 Seconds
// '%i Minute %s Seconds'                                  =>  49 Minute 36 Seconds
// '%h Hours                                               =>  11 Hours
// '%a Days                                                =>  468 Days
//////////////////////////////////////////////////////////////////////
function date_difference($date_1 , $date_2 , $differenceFormat = '%a' ){

    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    $interval = date_diff($datetime1, $datetime2);

    return $interval->format($differenceFormat);
}


/**
 * Time difference
 */
function time_difference($start_time, $end_time) {

  $to_time   = strtotime($end_time);
  $from_time = strtotime($start_time);
  $minutes   = (($to_time - $from_time) / 60);

  return ($minutes < 0 ? 0 : abs($minutes));
}




/**
 * Convert number month to word
 */
function convert_month($month){
  $date = DateTime::createFromFormat('!m', $month);
  return $date->format('F');
}


/**
 * When date is empty, give it today date
 */
function check_date($exist_date){

  if (empty($exist_date) || $exist_date == "0000-00-00") {
    $date = date('Y-m-d');
  } else {
    $date = $exist_date;
  }

  return $date;
}
