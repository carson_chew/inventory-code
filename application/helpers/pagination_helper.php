<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// ==================================================================
//
// Copyright (C) 2016 Carson Chew <chew_0212@hotmail.com>
//
// Everyone is permitted to copy and distribute verbatim or modified
// copies of this license document, and changing it is allowed as long
// as the name is changed.
//
// ------------------------------------------------------------------



function pagination($total_rows, $per_page = 10, $redirect_page, $uri_segment = 3, $tab_id = null, $page_query_string = FALSE, $per_page_pram = 'per_page'){

	$ci =& get_instance();

	$config['enable_query_strings'] = TRUE;

	$config['reuse_query_string']   = TRUE;
	$config['total_rows']           = $total_rows;
	$config['per_page']             = $per_page;
	$config['query_string_segment'] = $per_page_pram;
	$config['uri_segment']          = $uri_segment;
	$config['suffix']               = $tab_id;
	$config['page_query_string']    = $page_query_string;
	$config['base_url']             = site_url($redirect_page);


	// Load BS UI
	$ci->load->library('pagination');
	$ci->pagination->initialize($config);

	return $ci->pagination->create_links();
}


/**
 * Retrieve the last segment value on url
 */
function last_segment_value(){

	$ci = ci();
	$last         = $ci->uri->total_segments();
	$last_segment = $ci->uri->segment($last);

	return $last_segment;
}


/**
 * Retrieve the last segment key on url
 */
function last_segment_key(){

	$ci   = ci();
	$last = $ci->uri->total_segments();

	return $last;
}
