<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

	// Create product profile
	public function new_product_profile($foo_array){

		$sql = $this->db->insert('product_profile', $foo_array);

		if ($sql == true) {
			return true;
		}else{
			return false;
		}
	}


	// Update product profile
	public function update_product_profile($pid, $foo_array){

		$this->db->where('pp_id', $pid);
		$sql = $this->db->update('product_profile', $foo_array);

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}


	// Delete product profile
	public function delete_product_profile($pid){

		$this->db->where('pp_id', $pid);
		$sql = $this->db->delete('product_profile');

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}


	// Get single product profile
	public function single_product_profile($pid){

		$this->db->where('pp_id', $pid);
		$sql = $this->db->get('product_profile', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->row();
		} else {
			return false;
		}
	}


	// Show all product profile
	public function all_product($per_page_seg = "per_page"){

		$offset = get($per_page_seg);

		$this->db->limit(2, $offset);
		$this->db->order_by('pp_id', 'DESC');
		$this->db->where('cat_id = pp_cat_id');
		$sql = $this->db->get('product_profile, category', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->result();
		} else {
			return false;
		}
	}


	// Count all product profile
	public function count_all_product(){

		$this->db->where('cat_id = pp_cat_id');
		$this->db->select('pp_id');
		$sql = $this->db->count_all_results('product_profile, category');

		return $sql;
	}

}

/* End of file Product_model.php */
/* Location: ./application/models/Product_model.php */
