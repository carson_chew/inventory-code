<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message_handler_model extends CI_Model {


	/**
	 * @param  [int] = message PK
	 * @return [boolean OR message]
	 */
	public function get_msg_handler($msg_id){

		$this->db->where('mh_id', $msg_id);
		$sql = $this->db->get('message_handler');

		if ($sql->num_rows() != 0) {

			$row = $sql->row();

			$message = ['msg_type' => $row->mh_category, 'msg_desc' => $row->mh_msg];

			return $message;

		}else{
			return false;
		}
	}



	/**
	 * @param  [int] = message PK
	 * @return [boolean OR message]
	 */
	public function get_raw_msg($msg_id){

		$this->db->where('mh_id', $msg_id);
		$sql = $this->db->get('message_handler');

		if ($sql->num_rows() != 0) {

			$row = $sql->row();

			return $row->mh_msg;

		}else{
			return false;
		}
	}

}

/* End of file message_handler_model.php */
/* Location: ./application/models/message_handler_model.php */
