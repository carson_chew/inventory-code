<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

	// Create category profile
	public function new_category_profile($foo_array){

		$sql = $this->db->insert('category', $foo_array);

		if ($sql == true) {
			return true;
		}else{
			return false;
		}
	}


	// Update category profile
	public function update_category_profile($cat_id, $foo_array){

		$this->db->where('cat_id', $cat_id);
		$sql = $this->db->update('category', $foo_array);

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}


	// Delete category profile
	public function delete_category_profile($cat_id){

		$this->db->where('cat_id', $cat_id);
		$sql = $this->db->delete('category');

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}


	// Get single category profile
	public function single_category_profile($cat_id){

		$this->db->where('cat_id', $cat_id);
		$sql = $this->db->get('category', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->row();
		} else {
			return false;
		}
	}


	// Show all category profile
	public function all_category(){

		$this->db->order_by('cat_id', 'DESC');
		$sql = $this->db->get('category', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->result();
		} else {
			return false;
		}
	}


	// Count all category profile
	public function count_all_category(){

		$this->db->select('cat_id');
		$sql = $this->db->count_all_results('category');

		return $sql;
	}

}

/* End of file Category_model.php */
/* Location: ./application/models/Category_model.php */
