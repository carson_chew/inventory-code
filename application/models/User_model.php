<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	// Create user profile
	public function new_user_profile($foo_array){

		$sql = $this->db->insert('user_profile', $foo_array);

		if ($sql == true) {
			return true;
		}else{
			return false;
		}
	}


	// Update user profile
	public function update_user_profile($up_id, $foo_array){

		$this->db->where('up_id', $up_id);
		$sql = $this->db->update('user_profile', $foo_array);

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}


	// Delete user profile
	public function delete_user_profile($up_id){

		$this->db->where('up_id', $up_id);
		$sql = $this->db->delete('user_profile');

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}


	// Get single user profile
	public function single_user_profile($pid){

		$this->db->where('up_id', $up_id);
		$sql = $this->db->get('user_profile', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->row();
		} else {
			return false;
		}
	}


	// Get single user profile by name
	public function single_user_profile_by_name($username){

		$this->db->limit(1);
		$this->db->where('up_name', $username);
		$sql = $this->db->get('user_profile', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->row();
		} else {
			return false;
		}
	}

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */
